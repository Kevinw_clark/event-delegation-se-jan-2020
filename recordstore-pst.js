/*
X 1. Click on a record in the display rack to add it to the cart
    a. click handler to detect clicks on records
    b. Make a copy of the record div
        ?? How do we copy a node?
        -- node.cloneNode(true)
    c. append the copy into the cart
        -- cartEl.appendChild(recordElCopy)
X 2. Visually indicate that the record is selected
    a. Set some styles on the record div in the display rack
        ?? How do we use JS to set a style?
        node.className = ' ____ '
X 3. Click a record in the cart to remove it from the cart
    a. click handler
        -- Get a reference to the clicked record
    c. delete the record from the cart
    d. unselect the corresponding record in the display rack
*/

const addRecordToCart = function (evt) {
    if (evt.target.className != 'record') {
        return
    }

    const recordEl = evt.target
 
    // copy the record into the cart
    const recordElCopy = recordEl.cloneNode(true)
    const cartEl = document.querySelector('#cart')
    cartEl.appendChild(recordElCopy)

    // adds selected class
    recordEl.className += ' selected'
}

const removeRecordFromCart = function (evt) {
    if (evt.target.className != 'record') {
        return
    }

    const recordEl = evt.target
    const stockNum = recordEl.dataset.stocknum
    
    // remove record from cart
    const cartEl = document.querySelector('#cart')
    cartEl.removeChild(recordEl)

    // unselect the corresponding record in the rack
    const recordInRack = document.querySelector('.record[data-stocknum="' + stockNum + '"]')
    recordInRack.className = "record"
}

const displayRackEl = document.querySelector('#main')
displayRackEl.addEventListener('click', addRecordToCart)

const sidebarEl = document.querySelector('#sidebar')
sidebarEl.addEventListener('click', removeRecordFromCart)